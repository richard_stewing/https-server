// load modules
var https = require('https');
var fs = require('fs');
var express = require('express');
var cp = require('child_process');


//preperation
var reqHan = cp.fork('reqHan.js');
var app = express();
var port = 1234;

var options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};
//starts https server
https.createServer(options, app).listen(port);

//get handler
app.get('/', function(req, res){
fs.readFile('page.html', function(e, d){
	if(e){
		console.log(e);
	}else{
		var page = d;
		console.log(page.toString());
		res.send(page.toString());
	}
});
});

//put req handler
//with child process inwoked for faster execution 
app.put('/', function(req, res, data){
	//starts child which waits for a message
	var childjs=cp.fork('child.js');
	console.log('---------------------------------------------------------------------------------------------------------------');
	req.on('data', function (d){
		var dObj = d.toString();
		console.log(dObj+" from expressServer");
		dObj = JSON.parse(dObj);
		//sends message
		childjs.send({mes: dObj});
		
	});
	//eventlistener for the message back from the child
	childjs.on('message', function(d){
		//console.log(d);
		res.send({text : "answer :"+ d.mes, serverPort : port});
		//kills child
		childjs.kill();
	});
});
